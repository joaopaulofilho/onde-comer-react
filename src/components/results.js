import React, { useEffect, useContext, useState } from 'react'
import {
  Grid,
  Box,
  Heading,
  Text,
} from 'grommet';
import { get } from 'lodash'
import AppContext from '../contexts/app'
import { fetchBoardResultsAction } from '../store/actions'

const Results = React.memo(() => {
  const { date, board: { ranking, closed }, dispatch } = useContext(AppContext)

  const [loading, setLoading] = useState(true)

  useEffect(() => {
    if (ranking === undefined) {
      setLoading(true)
      fetchBoardResultsAction(dispatch, date)
    } else {
      setLoading(false)
    }
  }, [ranking, dispatch, date])

  return (
    <Box
      align='center'
    >
      {loading}

      {loading && (
        <Box
          gap='xlarge'
          size='xlarge'
          pad='large'
          width='medium'
          height='medium'
          direction='column'
          justify='center'
          alignContent='center'
          animation='pulse'
          data-testid='loading'
        >
          <Text textAlign='center'>Carregando resultados...</Text>
        </Box>
      )}

      {!loading && (
        <>
          <Heading margin='medium' level='2'>Vencedor {closed || '(em andamento)'}</Heading>

          <Grid
            rows={['xsmall', 'xsmall', 'xsmall']}
            columns={['small', 'small', 'small']}
            gap='medium'
            areas={[
              { name: 'first', start: [1, 0], end: [1, 2] },
              { name: 'second', start: [0, 1], end: [0, 2] },
              { name: 'third', start: [2, 2], end: [2, 2] },
            ]}
          >

            <Box gridArea='first' background='brand' align='center'>
              <Text size='medium' margin='small'>{get(ranking, '0.0')}</Text>
              <div>({get(ranking, '0.1') || '0'} votos)</div>
            </Box>

            <Box gridArea='second' background='accent-4' align='center'>
              <Text size='medium' margin='small'>{get(ranking, '1.0')}</Text>
              <div>({get(ranking, '1.1') || '0'} votos)</div>
            </Box>

            <Box gridArea='third' background='accent-3' align='center'>
              <Text size='medium' margin='small'>{get(ranking, '2.0')}</Text>
              <div>({get(ranking, '2.1') || '0'} votos)</div>
            </Box>
          </Grid>
        </>
      )}

    </Box>
  )
})

export default Results