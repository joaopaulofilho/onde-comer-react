import React from 'react'
import { render, fireEvent, screen, waitForElementToBeRemoved } from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { AppContextProvider } from '../../contexts/app'
import VoteForm from '../vote-form'

describe('Vote Form', function () {

  it('Will show alert for errors', function () {
    render(
      <AppContextProvider>
        <VoteForm closeCallback={() => null} />
      </AppContextProvider>
    )

    const cpf = '99999999999'
    const expectCpf = '999.999.999-99'
    const input = screen.getByTestId('cpf')
    userEvent.type(input, cpf)
    expect(input.value).toBe(expectCpf)

    global.alert = jest.fn()
    fireEvent.click(screen.getByRole('button', { name: /votar/i }))
    expect(global.alert).toHaveBeenCalledTimes(1)
  })

  it('Should close modal after save a vote', async function () {
    render(
      <AppContextProvider>
        <VoteForm closeCallback={() => null} />
      </AppContextProvider>
    )

    const cpf = '99999999999'
    const expectCpf = '999.999.999-99'
    const input = screen.getByTestId('cpf')
    fireEvent.change(input, { target: { value: cpf } })
    expect(input.value).toBe(expectCpf)

    const select = screen.getByTestId('restaurant')
    select.value = 1

    fireEvent.click(screen.getByRole('button', { name: /votar/i }))
    expect(screen.queryByRole(/vote onde sua fome/i)).not.toBeInTheDocument()
  })
})