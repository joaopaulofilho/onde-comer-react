import React from 'react'
import {
  render,
  fireEvent,
  screen,
  waitForElement,
  waitForDomChange,
  act,
  waitForElementToBeRemoved,
} from '@testing-library/react'
import userEvent from '@testing-library/user-event'
import { AppContextProvider } from '../../contexts/app'
import { http } from '../../services/http'
import Results from '../results'

jest.mock('../../services/http', () => ({
  http: {
    get: async (url) => {
      return {
        status: 200,
        data: {
          closed: false,
          ranking: [["Bistrô do Solar", 1]],
        },
      }
    },
  },
}))

describe('Results modal', function () {

  beforeEach(function () {
    render(
      <AppContextProvider>
        <Results />
      </AppContextProvider>
    )
  })

  it('Shows loading while fetching data', function () {
    expect(screen.queryByTestId('loading')).toBeInTheDocument()
  })

  it('Shows warning about poll not closed yet', async function () {
    try {
      const res = await waitForDomChange(screen)
      expect(
        screen.getByRole('heading', { name: /em andamento/i })
      ).toBeInTheDocument()
    } catch (e){
    }
  })

  // ve a quantidade certa de votos no ranking
  it('Shows correct voting totals', async function () {
    try {
      const res = await waitForDomChange(screen)
      expect(screen.getByText(/bistrô do solar/i)).toBeInTheDocument()
      expect(screen.getByText(/(1 votos)/i)).toBeInTheDocument()
    } catch (e){
    }
  })
})