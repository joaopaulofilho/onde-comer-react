import React, { useContext } from 'react'
import {
  Box,
  Text,
  Calendar,
} from 'grommet'
import moment from 'moment'
import AppContext from '../contexts/app'
import { changeDateAction } from '../store/actions'

const ChangeDate = React.memo(({ closeCallback }) => {
  const { date, dispatch } = useContext(AppContext)

  return (
    <Box
      direction='row'
      gap='xlarge'
      margin='xlarge'
      justify='center'
      alignContent='between'
      align='center'
    >
      <Text>
        {`Atualmente, a data para votação é ${moment(date).format('DD/MM/YYYY')}`}
      </Text>

      <Calendar
        local='pt-BR'
        size="small"
        date={moment(date).toDate().toISOString()}
        onSelect={date => {
          changeDateAction(dispatch, date)
          closeCallback()
        }}
      />

    </Box>
  )
})

export default ChangeDate