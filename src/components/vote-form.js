import React, { useContext, useState, useCallback, useEffect } from 'react'
import {
  Box,
  Heading,
  Button,
  FormField,
  MaskedInput,
  Select,
  Text,
} from 'grommet'
import moment from 'moment'
import AppContext from '../contexts/app'
import { getRestaurantsAction, addVoteAction } from '../store/actions'

const VoteForm = React.memo(({ closeCallback }) => {
  const { date, dispatch } = useContext(AppContext)

  const [restaurant, setRestaurant] = useState(0)
  const [cpf, setCpf] = useState('')
  const [restaurants, setRestaurants] = useState([])

  useEffect(() => {
    if (!restaurants.length) {
      getRestaurantsAction(dispatch, date)
        .then(setRestaurants)
    }
  }, [restaurants, dispatch, date])

  const saveVoteCallback = useCallback(async () => {
    const success = await addVoteAction(dispatch, { cpf, restaurant, date })

    if (success) {
      closeCallback()
    }
  }, [closeCallback, cpf, restaurant, dispatch, date])

  return (
    <Box
      background='light-3'
      direction='column'
      justify='center'
      align='center'
      round='small'
      pad='large'
      flex
    >
      <Heading
        margin='small'
        level='4'
      >
        Para onde sua fome te leva?
      </Heading>

      <Text
        color='light-7'
        size='14px'
        weight={200}
        margin='small'
      >
        voto para {moment(date).format('DD/MM/YYYY')}
      </Text>

      <Box
        align='start'
        flex={true}
      >
        <FormField label="CPF">
          <MaskedInput
            name='cpf'
            size='small'
            data-testid='cpf'
            onChange={e => setCpf(e.target.value)}
            mask={[
              {
                length: 3,
                regexp: /[0-9]/,
                placeholder: '___',
              },
              { fixed: '.' },
              {
                length: 3,
                regexp: /[0-9]/,
                placeholder: '___',
              },
              { fixed: '.' },
              {
                length: 3,
                regexp: /[0-9]/,
                placeholder: '___',
              },
              { fixed: '-' },
              {
                length: 2,
                regexp: /[0-9]/,
                placeholder: '__',
              },
            ]}
          />

        </FormField>

        <FormField label="Restaurantes">
          <Select
            name='restaurant'
            options={restaurants}
            labelKey='name'
            gap='small'
            data-testid='restaurant'
            onChange={({ option: { value } }) => {
              setRestaurant(value)
            }}
          />
        </FormField>

      </Box>

      <Button
        margin='small'
        size='small'
        label='Votar'
        alignSelf='center'
        onClick={saveVoteCallback}
      />

    </Box>
  )
})

export default VoteForm