import { http } from './http'

const addVotes = ({ cpf, restaurant, date }) =>
  http.post('/votes/add', { cpf, restaurant, date })

const getRestaurants = date => http.get(`/restaurants?date=${date}`)

const fetchResults = date => http.get(`/votes/results?date=${date}`)


export const RestaurantsService = {
  get: getRestaurants,
}

export const VotesService = {
  add: addVotes,
  fetchResults: fetchResults,
}