
export const messages = {
  'User already voted': 'Você já votou hoje',
  'Invalid user': 'CPF inválido',
  'Finished polls': 'Votação encerrada',
}