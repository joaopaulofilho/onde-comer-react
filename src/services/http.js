import axios from 'axios'

export const http = axios.create({
  baseURL: process.env.API_URL || 'http://localhost:3030',
})
