import React, { useReducer } from 'react'
import { reducer, initialState } from '../store/reducer'

const AppContext = React.createContext()

export const AppContextProvider = props => {
  const [state, dispatch] = useReducer(reducer, initialState)

  return (
    <AppContext.Provider value={{
      ...state,
      dispatch,
    }}>
      {props.children}
    </AppContext.Provider>
  )
}

export default AppContext
