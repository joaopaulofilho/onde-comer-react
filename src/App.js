import React from 'react';
import { Grommet } from 'grommet';
import theme from './themes'
import { AppContextProvider } from './contexts/app'
import Home from './pages/home'


const App = () => {
  return (
    <AppContextProvider>
      <Grommet
        theme={theme}
        themeMode={theme.mode}
        background={theme.background}
        full={theme.full}
      >
        <Home />
      </Grommet>
    </AppContextProvider>
  )
}

export default App;
