import React from 'react'
import { render, fireEvent, screen, waitForElementToBeRemoved } from '@testing-library/react';
import Home from '../home';
import { AppContextProvider } from '../../contexts/app'

describe('Home Page', function () {
  let renderer

  beforeEach(function () {
    renderer = render(
      <AppContextProvider>
        <Home />
      </AppContextProvider>
    )
  })

  it('Should render', function() {
    const header = screen.getByText(/onde vamos almoçar/i)
    expect(header).toBeInTheDocument()
  })

  it('Can open & close modal', async function () {
    const heading = /para onde sua fome te leva/i

    fireEvent.click(screen.getByText(/quero votar/i))
    const modal = screen.getByText(heading)
    expect(modal).toBeInTheDocument()

    fireEvent.keyPress(screen.getByRole('heading', { name: heading }), { key: 'Escape' })
    expect(screen.queryByRole(heading)).not.toBeInTheDocument()
  })

})