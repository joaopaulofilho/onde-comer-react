import React, { useState, useCallback, useEffect, useContext } from 'react'
import {
  Box,
  Heading,
  Button,
  Layer,
  Anchor,
} from 'grommet';
import moment from 'moment'
import AppContext from '../contexts/app'
import VoteForm from '../components/vote-form'
import Results from '../components/results'
import ChangeDate from '../components/change-date'

const Home = React.memo(() => {
  const { date } = useContext(AppContext)

  const [showVoteForm, setShowVoteForm] = useState(false)
  const closeModalCallback = useCallback(() => setShowVoteForm(false), [setShowVoteForm])

  const [showResults, setResults] = useState(false)
  const closeResultsCallback = useCallback(() => setResults(false), [setResults])

  const [showChangeDate, setChangeDate] = useState(false)
  const closeChangeDateCallback = useCallback(() => setChangeDate(false), [setChangeDate])

  return (
    <>
      <Heading margin='xlarge' textAlign='center'>Onde vamos almoçar hoje?</Heading>

        <Box
          direction='row'
          justify='center'
          align='center'
          gap='xlarge'
        >
          <Button
            size='large'
            gap='xlarge'
            label='Quero Votar'
            primary
            onClick={() => setShowVoteForm(true)}
          />

          <Button
            size='large'
            gap='xlarge'
            label='Resultado: Onde Vamos?'
            onClick={() => setResults(true)}
          />
        </Box>

        <Box
          direction='row'
          gap='xlarge'
          margin='xlarge'
          justify='center'
          alignContent='between'
          align='center'
        >
          <Anchor onClick={() => setChangeDate(true)}>
            {`Votação para o dia ${moment(date).format('DD/MM/YYYY')}`}
          </Anchor>
        </Box>

        {showVoteForm && (
          <Layer
            onEsc={closeModalCallback}
            onClickOutside={closeModalCallback}
          >
            <VoteForm
              closeCallback={closeModalCallback}
            />
          </Layer>
        )}

        {showResults && (
          <Layer
            onEsc={closeResultsCallback}
            onClickOutside={closeResultsCallback}
          >
            <Results />
          </Layer>
        )}

        {showChangeDate && (
          <Layer
            onEsc={closeChangeDateCallback}
            onClickOutside={closeChangeDateCallback}
          >
            <ChangeDate
              closeCallback={closeChangeDateCallback}
            />
          </Layer>
        )}
    </>
  )
})

export default Home