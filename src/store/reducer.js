import moment from 'moment'

export const initialState = {
  date: moment().format('YYYY-MM-DD'),
  board: { closed: undefined, ranking: undefined },
  // restaurants: [],
}

export const reducer = (state, { type, payload }) => {
  switch (type) {
    case 'save-date':
      return { ...state, date: payload }

    // case 'save-restaurants':
    //   return { ...state, restaurants: payload }

    case 'save-results':
      return { ...state, board: payload }

    case 'empty-results':
      return { ...state, board: { closed: undefined, ranking: undefined }}

    default:
      console.log('REDUCER TYPE ERROR', { type, payload });
      throw new Error('Invalid action type')
  }
}
