import { get } from 'lodash'
import moment from 'moment'
import {
  RestaurantsService,
  VotesService,
} from '../services'
import { messages } from '../services/messages'

const message = msg => {
  if (messages[msg]) {
    alert(messages[msg])
  } else {
    alert('Erro ao executar esta ação')
  }
}

export const getRestaurantsAction = async (dispatch, date) => {
  try {
    const { data: { list: restaurants }} = await RestaurantsService.get(date)

    if (!restaurants) {
      throw new Error('Nenhum restaurante disponível')
    }

    // dispatch({ type: 'save-restaurants', payload: restaurants })
    return restaurants
  } catch (err) {
    console.log('GET RESTAURANTS', err)
    message(get(err, 'response.data.message'))
  }
}

export const addVoteAction = async (dispatch, { cpf, restaurant, date }) => {
  if (!cpf) {
    return alert('Você deve informar seu CPF corretamente')
  }

  if (!restaurant) {
    return alert('Você deve escolher um restaurante')
  }

  try {
    const { status, data } = await VotesService.add({
      cpf: cpf.replace(/[\.\-]*/g, ''),
      restaurant,
      date,
    })

    dispatch({ type: 'empty-results' })

    return status === 201
  } catch (err) {
    console.log('ADD VOTE', err);
    message(get(err, 'response.data.message'))
  }
}

export const fetchBoardResultsAction = async (dispatch, date = moment().format('YYYY-MM-DD')) => {
  try {
    const { status, data } = await VotesService.fetchResults(date)
    dispatch({ type: 'save-results', payload: data })
  } catch (err) {
    message(get(err, 'response.data.message'))
  }
}

export const changeDateAction = (dispatch, newDate) => {
  dispatch({ type: 'save-date', payload: moment(newDate).format('YYYY-MM-DD') })
  dispatch({ type: 'empty-results' })
}