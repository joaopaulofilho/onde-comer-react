
const theme = {
  global: {
    colors: {
      light: '#F8F8F8',
      text: {
        light: 'rgba(0, 0, 0, 0.87)',
      },
    },
  },
  background: 'brand',
  mode: 'dark',
  full: true,
}

export default theme