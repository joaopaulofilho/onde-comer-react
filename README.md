# Onde Vamos Comer?
Aplicação para votação de restaurantes para almoçar com seus colegas e/ou amigos.

## Requisitos
- node v12+
- npm v6+
- [Express API](https://gitlab.com/joaopaulofilho/onde-comer-express) já rodando no mesmo ambiente de desenvolvimento

## Iniciar o app
- npm start
- Visitar http://localhost:3000/

## Como usar
Você tem disponível 3 opções para utilização:

### Votar
- Clique no botão "Quero Votar".
- Preencha seu CPF (veja a lista de CPFs válidos na seção "CPFs Válidos").
- escolha um restaurante.


### Ver resultado
- Clique no botão "Resultado: Onde Vamos?" para abrir o restaurante vencedor da votação.
- Caso a votação ainda não esteja encerrada (faltando um ou mais usuários para votar), a tela de resultados indicará com o texto "em andamento".


### Mudar dia da votação
- Mudar o dia da votação permite ver os resultados de outros dias e também votar nos próximos dias da semana.
- Clique no texto "Votação para o dia (...)" e escolha uma data.
- Para ver um restaurante vencedor, escolha a data de 06/09/2020 e clique para ver resultados.


### CPFs Válidos
São 9 usuários pré-cadastrados e somente eles podem votar.

- CPF: 11111111111
- CPF: 22222222222
- CPF: 33333333333
- CPF: 44444444444
- CPF: 55555555555
- CPF: 66666666666
- CPF: 77777777777
- CPF: 88888888888
- CPF: 99999999999

## Oportunidades para melhorias
- tela de login
- melhor cobertura de testes automatizados
- exibir erros ao usuário com layout/ui/ux, ao invés de alert
- bloquear votação em datas passadas
- bloquear votação para os dias seguintes